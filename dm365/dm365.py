from __future__ import print_function

import time

import socket
BUFFER_SIZE = 64

"""
Adapted for dm365 from https://github.com/SivyerLab/pyCrafter4500 (Alexander Tomlinson)
Some of the functions are litteraly copied from his work.

DM365 is the controller chip on the Lighcrafter Evaluation Module.

Docs: http://www.ti.com/lit/ug/dlpu007d/dlpu007d.pdf

To connect to Lighcrafter Evaluation Module, install USB RNDIS/Ethernet gadget driver.
"""

__version__ = '1.0'


def conv_len(a, l):
    """
    Function that converts a number into a bit string of given length

    :param a: number to convert
    :param l: length of bit string
    :return: padded bit string
    """
    b = bin(a)[2:]
    padding = l - len(b)
    b = '0' * padding + b
    return b


def bits_to_bytes(a, reverse=True):
    """
    Function that converts bit string into a given number of bytes

    :param a: bites to convert
    :param reverse: whether or not to reverse the byte list
    :return: list of bytes
    """
    bytelist = []

    # check if needs padding
    if len(a) % 8 != 0:
        padding = 8 - len(a) % 8
        a = '0' * padding + a

    # convert to bytes
    for i in range(len(a) // 8):
        bytelist.append(int(a[8 * i:8 * (i + 1)], 2))

    if reverse:
        bytelist.reverse()
    return bytelist


class dm365(object):
    """
    Class representing dm module
    """
    def __init__(self, tcp):
        """
        connects tcp

        :param tcp: socket tcp connection
        """
        self.tcp = tcp

    def command(self,
                mode,
                com1,
                com2,
                data=None):
        """
        Sends a command to the dm
        :param mode: whether reading or writing
        :param com1: command 1
        :param com2: command 2
        :param data: data to pass with command
        """

        buffer = []

        if mode == 'r':
            packet_type = 0x04  # host read command
        else:
            packet_type = 0x02  # host write command

        # flags
        flag = 0x00

        # payload length
        data_len = conv_len(len(data), 16)
        data_len = bits_to_bytes(data_len)

        # packet structure (w/ number of bytes):
        # - packet_type     (1)
        # - command 1       (1)
        # - command 2       (1)
        # - flags           (1)
        # - payload length  (2)
        # - data payload    (N)
        # - checksum        (1)

        buffer.append(packet_type)
        buffer.append(com1)
        buffer.append(com2)
        buffer.append(flag)
        buffer.extend(data_len)

        # if data fits into single buffer, write all and fill
        if len(buffer) + len(data) <= BUFFER_SIZE:
            for i in range(len(data)):
                buffer.append(data[i])

            # checksum
            buffer.append(sum(buffer) & 0xFF)

            # to bytes
            buffer = bytes(buffer)

            self.tcp.send(buffer)

        # done writing, read feedback from dm
        try:
            self.ans = self.tcp.recv(BUFFER_SIZE)
            # self.read_reply()
        except USBError as e:
            print('USB Error:', e)

        time.sleep(0.02)

    def read_reply(self):
        """
        Reads in reply
        """
        for i in self.ans:
            print(hex(i), '(', i, ')')
        print(' ')

    def set_black_test_pattern(self):
        """
        Sets the default internal test pattern as solid black.
        """
        self.command('w', 0x01, 0x03, [0x01])

    def set_display_mode(self, mode='test_pattern'):
        """
        Selects the display mode for the projector.

        :param mode: 1 = test pattern mode
                     2 = HDMI video mode
        """
        modes = ['', 'test_pattern', 'video']
        if mode in modes:
            mode = modes.index(mode)

        self.command('w', 0x01, 0x01, [mode])

    def set_black(self):
        """
        Sets projector output to black by setting display mode to test pattern which is solid black
        """
        self.set_display_mode()

    def initialize(self):
        """
        Initializes projector by setting default internal pattern as black and put it on
        """
        self.set_video_mode(color=1, bit_depth=8, frame_rate=60)
        self.set_black_test_pattern()
        self.set_black()

    def get_video_mode(self):
        """
        Gets the video mode of the projector.
        """
        self.command('r', 0x02, 0x01, [])
        if self.ans[0] == 0x05:
            return ['', 'rgb', 'red', 'green', 'blue'][int(self.ans[8])]
        return 0

    def set_video_mode(self, frame_rate=60, bit_depth=8, color=1):
        """
        Selects the video mode for the projector.

        :param frame_rate: 15, 30, 40, 50 or 60 Hz.
        :param bit_depth: range 1 - 8.
        :param color: 1 = RGB
                      2 = monochrome Red
                      3 = monochrome Green
                      4 = monochrome Blue
        """
        frame_rate = conv_len(frame_rate, 8)
        bit_depth = conv_len(bit_depth, 8)
        color = conv_len(color, 8)
        payload = bits_to_bytes(color + bit_depth + frame_rate)
        self.command('w', 0x02, 0x01, payload)

    def enable(self):
        """
        Puts LCR into HDMI video mode.
        """
        self.set_display_mode('video')

    def disable(self):
        """
        Puts LCR into standby mode (black test pattern).
        """
        self.set_black()

    def current_color_mode(self):
        """
        Gets LCR current color mode.
        """
        return self.get_video_mode()

    def color_mode(self, color="rgb"):
        """
        Puts LCR into given color mode.
        """
        colors = ['', 'rgb', 'red', 'green', 'blue']
        if color in colors:
            color = colors.index(color)

        self.set_video_mode(color=color, bit_depth=8, frame_rate=60)

    def disconnect(self):
        """
        Releases LCR USB connection.
        """
        self.tcp.close()
        del self.tcp
        del self

def connect(ip='192.168.1.100', port=21845):
    """
    Connects to LCR EM via TCP over USB.
    """
    skt = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    skt.settimeout(3.0)

    try:
        skt.connect((ip, port))
        lcr = dm365(skt)
        lcr.initialize()
        print('LCR EM connected!')
        return lcr

    except socket.timeout:
        print('TCP timeout error: no LCR EM found.')
        return None