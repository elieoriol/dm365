from distutils.core import setup

setup(
    name = 'dm365',
    packages = ['dm365'], # this must be the same as the name above
    version = '1.0',
    description = 'A python interface to communicate over USB with the TI Lightcrafter Evaluation Module',
    author = 'Elie Oriol',
    author_email = 'elie.oriol@polytechnique.edu',
    url = 'https://github.com/SivyerLab/pyCrafter4500',
    download_url = 'https://github.com/SivyerLab/pyCrafter4500/archive/0.5.tar.gz',
    keywords = 'lightcrafter evaluation module dm 365 projector texas instruments',
    install_requires=[],
    license='MIT',
)