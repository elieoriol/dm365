Adapted from the work of Alexander Tomlinson (Sivyer Lab) suited to what we do in the Behnia Lab. A lot of the code has been kept as written in pycrafter4500. Below is an adapted readme for our version.

# dm365

This is an unofficial API to communicate with the DM 365 controller chip (e.g. Texas Instruments Lightcrafter Evaluation Module).

Code adapted from pycrafter4500 (https://github.com/SivyerLab/pyCrafter4500) itself adapted from https://github.com/csi-dcsc/Pycrafter6500

TI DM 365 documentation can be found at: http://www.ti.com/lit/ug/dlpu007d/dlpu007d.pdf

To connect to the LCR EM, the USB_RNDIS driver is required. The recommended way to do this is
with the Windows Device Manager. If the projector can already be found as a USB Ethernet/RNDIS Gadget under Network Adapters, you should be good to go. Otherwise, you need to find it, certainly in the Universal Serial Bus devices. Double click on it and go in the Driver tab. Click Update Driver, then Browse my computer for driver software, then Let me pick from a list of available drivers on my computer, and choose USB Ethernet/RNDIS Gadget.


## Install

You can install it locally

```bash
git clone git@gitlab.com:elieo/dm365.git
cd dm365
pip install -e .
```


## Usage

Import and connects to a LCR EM via TCP over USB if one is connected. Its IP should be by default '192.168.1.100', and you can choose the port.
Warning: if you've just connected your LCR EM, it can take time to set up its IP properly. You may wait a little bit and check if things evolve using the ipconfig command on the Windows Command Prompt. At the end you should at least obtain an IP beginning with '192.168.'

```python
import dm365
lcr = dm365.connect(ip='192.168.1.100', port=21845)
```

Putting into fast standby (solid black internal test pattern) and fast waking up

```python
lcr.disable()
lcr.enable()
```

To set the projector to RGB, monochrome Red, monochrome Green, monochrome Blue (that can work at 180 Hz with framepacking)

```python
lcr.color_mode(color='rgb')
lcr.color_mode(color='red')
lcr.color_mode(color='green')
lcr.color_mode(color='blue')
```

To get current color mode (returns 'rgb', 'red', 'green', 'blue' or '' if none of the previous)

```python
color_mode = lcr.current_color_mode()
```

To disconnect the projector and free the TCP connection (it will delete the object instance)

```python
lcr.disconnect()
```

There are other commands already implemented in the dm365 class.
If you wish to send other commands, this can be done using the class as well to write your own. See source documentation for further details.

```python
from dm365 import dm365, connect
from dm365 import bits_to_bytes, conv_len

lcr = dm365.connect()
lcr.command('w', CMD1, CMD2, payload)
```
